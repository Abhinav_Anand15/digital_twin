import React from "react";
import Data from "./Components/Data/Data";
import Home from "./Components/Home/Home";

export default function App() {
  return (
    <div>
      <div>
        {/* <Data /> */}
        <Home />
      </div>
    </div>
  );
}
