import React, { useEffect, useState } from "react";
import { FaTemperatureLow } from "react-icons/fa";
import styles from "./Data.module.css";
import { WiHumidity } from "react-icons/wi";
import { TbBrandCarbon } from "react-icons/tb";
import { FaPeopleGroup } from "react-icons/fa6";
import { FaRegLightbulb } from "react-icons/fa";
import { BsSpeedometer } from "react-icons/bs";
import axios from "axios";

export default function Data() {
  const [data, setData] = useState({});
  const [censor, setCensor] = useState({});

  const fetchData = async () => {
    try {
      const [peopleData, sensorData] = await Promise.all([
        axios.get("https://dt-be.vercel.app/latestRow?data=people_count"),
        axios.get("https://dt-be.vercel.app/latestRow?data=sensors"),
      ]);

      setData(peopleData.data);
      setCensor(sensorData.data);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  useEffect(() => {
    fetchData(); // Fetch data initially when component mounts

    const intervalId = setInterval(fetchData, 10000); // Fetch data every 10 seconds

    return () => clearInterval(intervalId); // Cleanup function to clear interval
  }, []);

  return (
    <div className={styles.container}>
      <div className={styles.container00}>
        <div className={styles.container0}>
          <label className={styles.l1}>
            <label className={styles.l2}>Digital Twin</label> Information
          </label>
        </div>
        <div className={styles.hr1}>
          <div className={styles.hr}></div>
        </div>
        <div className={styles.container1}>
          <div className={styles.main1}>
            <div className={styles.outer}>
              <label className={styles.l6}>Temperature °C</label>
              <div className={styles.main3}>
                <div className={styles.icon}>
                  <FaTemperatureLow />
                  <label className={styles.l4}>{censor.temperature}</label>
                </div>
              </div>
            </div>
            <div className={styles.outer}>
              <label className={styles.l6}>Humidity %</label>
              <div className={styles.main3}>
                <div className={styles.icon}>
                  <WiHumidity />
                  <label className={styles.l4}>
                    {censor.humidity &&
                      Number(censor.humidity).toLocaleString(undefined, {
                        maximumFractionDigits: 2,
                      })}
                  </label>
                </div>
              </div>
            </div>
            <div className={styles.outer}>
              <label className={styles.l6}>Carbon PPM</label>
              <div className={styles.main3}>
                <div className={styles.icon}>
                  <TbBrandCarbon />
                  <label className={styles.l4}>{censor.eco2}</label>
                </div>
              </div>
            </div>
          </div>
          <div className={styles.main2}>
            <div className={styles.outer}>
              <label className={styles.l6}>People Count</label>
              <div className={styles.main3}>
                <div className={styles.icon}>
                  <FaPeopleGroup />
                  <label className={styles.l4}>{data.person_count}</label>
                </div>
              </div>
            </div>

            <div className={styles.outer}>
              <label className={styles.l6}>Light</label>
              <div className={styles.main3}>
                <div className={styles.icon}>
                  <FaRegLightbulb />
                  <label className={styles.l4}>{censor.ldr}</label>
                </div>
              </div>
            </div>
            <div className={styles.outer}>
              <label className={styles.l6}>Pressure hPa</label>
              <div className={styles.main3}>
                <div className={styles.icon}>
                  <BsSpeedometer />
                  <label className={styles.l4}>
                    {censor.pressure &&
                      Number(censor.pressure).toLocaleString(undefined, {
                        maximumFractionDigits: 2,
                      })}
                  </label>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
