import React from "react";
import styles from "./Home.module.css";
import Data from "../Data/Data";
import Navbar1 from "../Navbar/Navbar";

export default function Home() {
  return (
    <div className={styles.main}>
      <Navbar1 />
      <Data />
    </div>
  );
}
