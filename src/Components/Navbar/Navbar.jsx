import React from "react";
import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import NavDropdown from "react-bootstrap/NavDropdown";
import styles from "./Navbar.module.css";
import "bootstrap/dist/css/bootstrap.min.css";
import logo from "../../assets/logo1.png";

export default function Navbar1() {
  return (
    <div>
      {/* Include the BasicExample component */}
      <BasicExample />
    </div>
  );
}

function BasicExample() {
  return (
    <Navbar expand="lg" className={styles.main}>
      <img className={styles.imgr} src={logo} alt="" />
      <Container className={styles.container}>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav href="#link" className={styles.nav}></Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}
